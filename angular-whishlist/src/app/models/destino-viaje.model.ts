import {v4 as uuid} from 'uuid';

export class  DestinoViaje  {
    selected: boolean;
    servicios: string[];
    id = uuid();

  
  constructor(public nombre: string, public imagenUrl: string,public votes: number = 0) {
      
       this.servicios = ['piscina', 'desayuno'];
  }

  public setSelected(s: boolean) {
    this.selected = s;
  }
  
  public isSelected():boolean {
    return this.selected;
  }

  voteUp(){
    this.votes++;
  }
  
  voteDown(){
    this.votes--;
  }

}